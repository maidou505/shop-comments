package com.hmdp.dto;

import lombok.Data;

/**
 * 登录信息DTO
 *
 * @author zhanhong
 */
@Data
public class LoginFormDTO {
    /**
     * 手机号码
     */
    private String phone;

    /**
     * 短信验证码
     */
    private String code;

    /**
     * 密码
     */
    private String password;
}
