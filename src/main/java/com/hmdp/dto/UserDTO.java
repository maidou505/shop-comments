package com.hmdp.dto;

import lombok.Data;

@Data
public class UserDTO {
    private Long id;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String icon;
}
