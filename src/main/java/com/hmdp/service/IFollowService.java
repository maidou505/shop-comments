package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.dto.Result;
import com.hmdp.entity.Follow;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface IFollowService extends IService<Follow> {

    /**
     * 关注用户
     *
     * @param followUserId 被关注用户的id
     * @param isFollow     true：关注；false：取关
     * @return
     */
    Result follow(Long followUserId, Boolean isFollow);

    Result isFollow(Long followUserId);

    /**
     * 共同好友功能
     *
     * @param id
     * @return
     */
    Result followCommons(Long id);
}
