package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.dto.Result;
import com.hmdp.entity.Shop;


/**
 * ishop服务
 *
 * @author zhanhong
 * @date 2023/01/31
 */
public interface IShopService extends IService<Shop> {

    Result queryById(Long id);

    Result update(Shop shop);

    /**
     * 附近商铺
     *
     * @param typeId  商铺类型ID
     * @param current 页号
     * @param x       经度
     * @param y       纬度
     * @return
     */
    Result queryShopByType(Integer typeId, Integer current, Double x, Double y);
}
