package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.dto.Result;
import com.hmdp.entity.Blog;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface IBlogService extends IService<Blog> {

    Result queryHotBlog(Integer current);

    Result queryBlogById(Long id);

    /**
     * 点赞功能
     *
     * @param id blog的ID
     * @return
     */
    Result likeBlog(Long id);

    /**
     * 点赞排行榜功能
     *
     * @param id blog的ID
     * @return
     */
    Result queryBlogLikes(Long id);

    Result queryBlogByUserId(Long id, Integer current);

    Result saveBlog(Blog blog);

    Result queryBlogOfFollow(Long max, Integer offset);
}
