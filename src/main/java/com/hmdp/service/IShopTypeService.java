package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.dto.Result;
import com.hmdp.entity.ShopType;

/**
 * 商店类型服务接口
 *
 * @author zhanhong
 * @date 2023/01/31
 */
public interface IShopTypeService extends IService<ShopType> {

    /**
     * 查询类型列表
     *
     * @return {@link Result}
     */
    Result queryTypeList();
}
