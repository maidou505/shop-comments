package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.dto.Result;
import com.hmdp.entity.Voucher;

/**
 * 代金券服务
 *
 * @author zhanhong
 * @date 2023/01/31
 */
public interface IVoucherService extends IService<Voucher> {

    /**
     * 查询代金券of商店
     *
     * @param shopId 商店id
     * @return {@link Result}
     */
    Result queryVoucherOfShop(Long shopId);

    /**
     * 添加秒杀代金券
     *
     * @param voucher 代金券
     */
    void addSeckillVoucher(Voucher voucher);
}
