package com.hmdp.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.Result;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商店类型服务impl
 *
 * @author zhanhong
 * @date 2023/01/31
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryTypeList() {
        //1.从redis查询店铺类型缓存
        String key = "shoptype";
        String shopTypeJson = stringRedisTemplate.opsForValue().get(key);
        //2.判断是否存在
        if (StrUtil.isNotBlank(shopTypeJson)) {
            //3.存在 直接返回数据
            List<ShopType> shopTypes = JSONUtil.toList(shopTypeJson, ShopType.class);
            return Result.ok(shopTypes);
        }
        //4.不存在，查询数据库
        List<ShopType> sort = query().orderByAsc("sort").list();
        //5.判断数据库是否存在商铺类型
        if (sort == null) {
            //6.不存在
            return Result.fail("分类不存在");
        }
        //7. 存在，将查询到的信息存入redis
        stringRedisTemplate.opsForValue().set("shopType", JSONUtil.toJsonStr(sort));
        //8.返回
        return Result.ok(sort);
    }
}
