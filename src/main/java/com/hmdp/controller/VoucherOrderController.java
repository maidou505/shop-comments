package com.hmdp.controller;

import com.hmdp.dto.Result;
import com.hmdp.service.IVoucherOrderService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 代金券订单控制器
 *
 * @author zhanhong
 * @date 2023/01/31
 */
@RestController
@RequestMapping("/voucher-order")
public class VoucherOrderController {

    /**
     * 代金券订单服务
     */
    @Resource
    private IVoucherOrderService voucherOrderService;

    /**
     * 秒杀优惠券
     *
     * @param voucherId 优惠券id
     * @return {@link Result}
     */
    @PostMapping("seckill/{id}")
    public Result seckillVoucher(@PathVariable("id") Long voucherId) {
        return voucherOrderService.seckillVoucher(voucherId);
    }
}
