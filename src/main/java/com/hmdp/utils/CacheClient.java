package com.hmdp.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.hmdp.utils.RedisConstants.*;

/**
 * Redis工具类，用来实现对redis中的查询和写入
 * 提供解决缓存穿透和缓存击穿的方法
 *
 * @author zhanhong
 */
@Slf4j
@Component
public class CacheClient {

    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 创建一个线程池
     */
    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    public CacheClient(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 将任意Java类型的数据序列化成JSON并存储在String类型的key中，并可以设置TTL时间
     *
     * @param key
     * @param value
     * @param time
     * @param unit
     */
    public void set(String key, Object value, Long time, TimeUnit unit) {
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value), time, unit);
    }

    /**
     * 将任意Java类型的数据序列化成JSON并存储在String类型的key中，并可以设置逻辑过期时间，用于解决缓存击穿的问题
     *
     * @param key
     * @param value
     * @param time
     * @param unit
     */
    public void setWithLogicalExpire(String key, Object value, Long time, TimeUnit unit) {
        //设置逻辑过期
        RedisData redisData = new RedisData();
        redisData.setData(value);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));
        //写入redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData));
    }

    /**
     * 根据指定的可key查询缓存，并反序列化成指定类型，利用缓存空对象方法解决缓存穿透问题
     *
     * @param keyPrefix
     * @param id
     * @param type
     * @param dbFallBack
     * @param time
     * @param unit
     * @param <R>
     * @param <ID>
     * @return
     */
    public <R, ID> R queryWithPassTrough(String keyPrefix, ID id, Class<R> type,
                                         Function<ID, R> dbFallBack, Long time, TimeUnit unit) {
        //1.从redis查询商铺缓存
        String key = keyPrefix + id;
        String json = stringRedisTemplate.opsForValue().get(key);
        //2.判断是否存在
        if (StrUtil.isNotBlank(json)) {
            //3.存在 直接返回数据
            return JSONUtil.toBean(json, type);
        }
        //判断命中的是否是空值
        if (json != null) {
            //返回一个错误信息
            return null;
        }

        //4.不存在，根据id查询数据库
        R r = dbFallBack.apply(id);
        //5.判断数据库是否存在该id的商铺
        if (r == null) {
            //将空值写入redis
            stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            //6.不存在，返回错误
            return null;
        }
        //7.存在 写入redis
        this.set(key, r, time, TimeUnit.MINUTES);
        //8.返回
        return r;
    }

    /**
     * 根据指定的可key查询缓存，并反序列化成指定类型，利用逻辑过期方法解决缓存击穿问题
     *
     * @param keyPrefix
     * @param id
     * @param type
     * @param dbFallback
     * @param time
     * @param unit
     * @param <R>
     * @param <ID>
     * @return
     */
    public <R, ID> R queryWithLogicalExpire(String keyPrefix, ID id, Class<R> type,
                                            Function<ID, R> dbFallback, Long time, TimeUnit unit) {
        //1.从redis查询商铺缓存
        String key = keyPrefix + id;
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        //2.判断是否存在
        if (StrUtil.isBlank(shopJson)) {
            //3.不存在
            return null;
        }
        //4.命中，需要先把json反序列化为对象
        RedisData redisData = JSONUtil.toBean(shopJson, RedisData.class);
        LocalDateTime expireTime = redisData.getExpireTime();
        R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);
        //5.判断是否过期
        if (expireTime.isAfter(LocalDateTime.now())) {
            //5.1 未过期，直接返回店铺信息
            return r;
        }
        //5.2 已过期，需要缓存重建

        //6.缓存重建
        //6.1 获取互斥锁
        String lockKey = LOCK_SHOP_KEY + id;
        Boolean isLock = tryLock(lockKey);
        //6.2判断是否获取锁成功
        if (isLock) {
            //6.3获取锁成功，开启独立线程
            CACHE_REBUILD_EXECUTOR.submit(() -> {
                try {
                    //查询数据库
                    R r1 = dbFallback.apply(id);
                    //重建缓存
                    this.setWithLogicalExpire(key, r1, time, unit);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                } finally {
                    //释放锁
                    unLock(lockKey);
                }
            });
        }
        //6.4返回过期的商铺信息
        return r;
    }

    /**
     * 根据指定的可key查询缓存，并反序列化成指定类型，利用互斥锁方法解决缓存击穿问题
     *
     * @param keyPrefix
     * @param id
     * @param type
     * @param dbFallback
     * @param time
     * @param timeUnit
     * @param lockKeyPrefix
     * @param <R>
     * @param <ID>
     * @return
     */
    public <R, ID> R queryWithMutex(String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit timeUnit, String lockKeyPrefix) {
        // TODO 1.根据ID到Redis中去查询数据
        String json = stringRedisTemplate.opsForValue().get(keyPrefix + id);
        // TODO 2.如果查到了且不是空数据 则直接返回 isNotBlack ""返回false
        if (StrUtil.isNotBlank(json)) {
            // 将Json转化成对象返回
            return JSONUtil.toBean(json, type);
        }
        // TODO 3.判断redis中存储的是否是空对象 如果是空数据 返回错误信息
        if ("".equals(json)) {
            return null;
        }
        // TODO 4.如果没查到 则去获取锁，如果获取到锁，则查询数据库重建缓存，如果没有获取到锁，则等待后重新到缓存查询
        R r = null;
        try {
            if (!tryLock(keyPrefix + id)) {
                // 如果没有获取到锁，那么线程等待后重新调用该方法
                Thread.sleep(50);
                return queryWithMutex(keyPrefix, id, type, dbFallback, time, timeUnit, lockKeyPrefix);
            }
            // TODO 5.如果获取到锁，则去数据库查询
            r = dbFallback.apply(id);
            // TODO 6.数据库中如果没查到，则创建一个空对象存储到redis中
            if (r == null) {
                this.set(keyPrefix + id, "", time, timeUnit);
                return null;
            }
            // TODO 7.数据库中如果查到了，则将结果保存到Redis中
            this.set(keyPrefix + id, JSONUtil.toJsonStr(r), time, timeUnit);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            // TODO 8.释放锁
            unLock(keyPrefix + id);
        }
        return r;
    }

    /**
     * 缓存击穿问题解决办法之互斥锁：尝试得到锁（锁10秒）
     *
     * @param key
     * @return
     */
    private Boolean tryLock(String key) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", LOCK_SHOP_TTL, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    /**
     * 缓存击穿问题解决办法之互斥锁：释放锁
     *
     * @param key
     */
    private void unLock(String key) {
        stringRedisTemplate.delete(key);
    }
}
