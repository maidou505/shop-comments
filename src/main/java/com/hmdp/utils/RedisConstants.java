package com.hmdp.utils;

public class RedisConstants {
    /**
     * 短信验证码
     */
    public static final String LOGIN_CODE_KEY = "login:code:";

    /**
     * 短信验证码有效期：分钟
     */
    public static final Long LOGIN_CODE_TTL = 5L;

    /**
     * 用户登录token
     */
    public static final String LOGIN_USER_KEY = "login:token:";

    /**
     * token有效期：分钟
     */
    public static final Long LOGIN_USER_TTL = 30L;

    public static final Long CACHE_NULL_TTL = 2L;

    /**
     * 商铺缓存有效期：分钟
     */
    public static final Long CACHE_SHOP_TTL = 30L;

    /**
     * 商铺缓存key
     */
    public static final String CACHE_SHOP_KEY = "cache:shop:";

    /**
     * 锁商铺key
     */
    public static final String LOCK_SHOP_KEY = "lock:shop:";

    /**
     * 锁商铺时间
     */
    public static final Long LOCK_SHOP_TTL = 10L;

    /**
     * 库存key
     */
    public static final String SECKILL_STOCK_KEY = "seckill:stock:";

    /**
     * 点赞key
     */
    public static final String BLOG_LIKED_KEY = "blog:liked:";
    public static final String FEED_KEY = "feed:";

    /**
     * 商铺位置信息key
     */
    public static final String SHOP_GEO_KEY = "shop:geo:";
    public static final String USER_SIGN_KEY = "sign:";
}
