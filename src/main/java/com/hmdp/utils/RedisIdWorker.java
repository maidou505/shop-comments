package com.hmdp.utils;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @author zhanhong
 * @date 2023/01/31
 */
@Component
public class RedisIdWorker {

    /**
     * 起始时间戳
     */
    private static final long BEGIN_TIMESTAMP = 1640995200L;

    /**
     * 序列号的位数
     */
    private static final long COUNT_BITS = 32;

    private StringRedisTemplate stringRedisTemplate;

    public RedisIdWorker(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public long nextId(String keyPrefix) {
        //1.获取当前时间戳
        LocalDateTime now = LocalDateTime.now();
        //ZoneOffset.UTC
        long currentTimeStamp = now.toEpochSecond(ZoneOffset.of("+8"));
        long timestamp = currentTimeStamp - BEGIN_TIMESTAMP;

        //2.生成序列号
        // 这里需要说明一下Redis中保存这个自增值的键的命名
        // [1]首先应该有前缀 "icr:"
        // [2]业务名字，比如订单就是order
        // [3]日期，因为Redis单个键中值的最大值就是2^64 如果时间久了 肯定会超过这个数值，导致序列号无法继续自增
        // 因此可以每一天设置一个key 一天中订单不可能超过2^64
        // icr:order:2022:11:23 就是一个键，设置成这样的好处是可以根据月份，天和年进行分别查找，因为都是用：隔开了
        //2.1 获取当前日期，精确到天
        String date = now.format(DateTimeFormatter.ofPattern("yyyy:MM:dd"));
        //2.2 自增长：序列号从0自增，每次增加1
        Long serialCode = stringRedisTemplate.opsForValue().increment("icr:" + keyPrefix + ":" + date);

        //3.拼接并返回，时间戳由于需要到高32位，因此使用位运算 << 向左移动32位
        //那么此时时间戳的第32位都为0，然后进行和序列号按位与运算，可以将序列号拼接到时间戳的低32位中，最终形成id
        return timestamp << COUNT_BITS | serialCode;
    }
}
