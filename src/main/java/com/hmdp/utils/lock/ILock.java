package com.hmdp.utils.lock;

/**
 * 分布式锁
 */
public interface ILock {

    //尝试获取锁
    Boolean tryLock(long timeoutSec);

    //释放锁
    void unLock();

}
