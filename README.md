# 商铺点评

## 介绍

【仿 大众点评】
本项目是为游玩达人定制的前后端分离的软件，除了常用的增删改查实现外，还依托 Redis 实
现了对高并发业务的处理，主要实现了短信登录，优惠券秒杀，探店达人，好友关注，签到点赞等功能

## 软件架构

SpringBoot+MyBatisPlus+MySQL+Redis+SpringMVC+Nginx

## 安装教程

1. xxxx
2. xxxx
3. xxxx

## 使用说明

1. xxxx
2. xxxx
3. xxxx

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

## 遇到问题

### 项目启动报错：

错误信息：【NOGROUP No such key ‘stream.orders’ or consumer group ‘g1’ in XREADGROUP with GROUP option】
解决办法：https://blog.csdn.net/crayon0/article/details/128172014

创建一个Stream类型的[消息队列](https://so.csdn.net/so/search?q=消息队列&spm=1001.2101.3001.7020)，名为stream.orders

```
XGROUP CREATE stream.orders g1 0 MKSTREAM
#   XGROUP CREATE 队列名称  组名称  起始id  MKSTREAM不存在则自动创建

#   XGROUP 支持创建和销毁组，也支持管理consumer
#   XGROUP CREATE key groupname id|$ [MKSTREAM] [ENTRIESREAD entries_read]
#   MKSTREAM 可选选项，默认不加的话，如果指定的 stream 不存在会返回错误，加上之后不存在则会自动创建 stream
```

[分布式锁相关知识](https://blog.csdn.net/lyx7762/article/details/128077838)

- 缓存穿透
- 缓存雪崩
- 缓存击穿

